import torch.nn as nn
import torch.nn.functional as F
import torch


class Narx(nn.Module):
    """"""
    def block(in_feat, out_feat, normalize=True):
        layers = [nn.Linear(in_feat, out_feat)]
        if normalize:
            layers.append(nn.BatchNorm1d(out_feat, 0.8))
        layers.append(nn.LeakyReLU(0.2, inplace=True))
        return layers
        
    def __init__(self, exogenous_dim, nb_layers=2, order=2, exog_delay=1, 
                hidden_layer_size=10 ):
        """"""
        super(Narx, self).__init__()
        print(exogenous_dim)
        assert len(exogenous_dim) >=1, "dimension error for endogenous_dim parameter" 
        assert nb_layers >=1, "Error in nb_layers, cannot create a model with least than one FC layer!"
        self.exogenous_dim = exogenous_dim
        self.order = order
        self.exog_delay = exog_delay

        self.fc_layer_exog = nn.Linear(exogenous_dim[-1],hidden_layer_size)
        self.fc_layer_endog = nn.Linear(1, hidden_layer_size)
        self.activ = nn.LeakyReLU(0.2,inplace=True)
        
        self.fc_intermediate_layer = nn.Linear(hidden_layer_size, 1)
        self.fc_final_layer = nn.Linear(exogenous_dim[0] + order , 1)
        #self.activ2 = nn.Hardtanh(inplace=True)
        
        
    def forward(self, exogenous, endogenous):
        """"""
        #exogenous = exogenous.reshape((-1, exogenous.shape[0]))
        sig1 = self.fc_layer_exog(exogenous)
        sig1 = self.activ(sig1)
        
        #endogenous = endogenous.reshape((-1, endogenous.shape[0]))
        sig2 = self.fc_layer_endog(endogenous)
        sig2 = self.activ(sig2)
        
        sig = torch.cat([sig1, sig2])
        
        sig = self.fc_intermediate_layer(sig)
        sig = self.activ(sig)
        sig  = sig.reshape((-1, sig.shape[0]))
        
        sig = self.fc_final_layer(sig)
        #sig = self.activ2(sig)
        
        return sig[0]