import torch
import numpy as np


def normalize_sig(sig):
    mean = sig.mean()
    std = sig.std()
    return (sig - mean) / std
    
    

# change window_size into exog_order
class DataIter(object):
    """"""
    
    def __init__(self, signal_input=None, signal_output=None,
                 window_size=10, order=2,  exog_delay=1, zero_padding=True, 
                batch_size=10):
        
        #assert signal_input.shape[0] == signal_output.shape[0], "Dimension error, signal_input and output should have the same size"
        
            
            
        signal_input_tmp = signal_input[exog_delay:]
        endogenous_signal = signal_output[:-1 * exog_delay]


        if zero_padding and exog_delay + order > window_size:
            # complete signal using zero paddding
            zeros = np.zeros((exog_delay))
            endogenous_signal = np.append(zeros, 
                                              endogenous_signal)
            endogenous_signal = endogenous_signal[:,np.newaxis]
            signal_input_tmp = signal_input

        signal_input = signal_input_tmp
        target = signal_output[window_size-1:]
        
        
        self.signal_input = torch.tensor(signal_input).type('torch.FloatTensor')
        self.endogenous_signal = torch.tensor(endogenous_signal).type('torch.FloatTensor')
        self.target = torch.tensor(target).type('torch.FloatTensor')
        
        self.window_size = window_size
        self.exog_delay = exog_delay
        self.order = order
        # get length of object
        signal_length = self.signal_input.shape[0]
        
        self._length =  signal_length  -  exog_delay - window_size
        
        assert self._length >0, "error : got window_size and exog_delay too important in comparaison to signal_length"
            
        self._nb_iter = 0
        self._is_dynamic = False
        
        if batch_size > 1:
            pass # to continue ...
        
        # if user decide to build endogenous variable 'on the run'
        self.predicted = torch.cat([self.target[:order],
                                    torch.zeros((target.shape[0]-order, 1))], axis=0)
        self._nb_idx_predicted = order
        
    def __getitem__(self, index):
        
        def getitem(sub_array, index, lim, length):
            """"""
            if abs(index) >= length:
                raise IndexError("index exceed size of object")
            elif index == -1 :
                sub_array = sub_array[-1*lim: ]
            elif length - index < lim:

                sub_array = sub_array[index:]

            else:
                lower_lim = index
                upper_lim = index + lim
                sub_array = sub_array[lower_lim:upper_lim ]
            return sub_array
        
        sub_input_array = getitem(self.signal_input, index, self.window_size, 
                                 self._length)
        endogenous_array = getitem(self.endogenous_signal, index, self.order, 
                                  self._length)
        
        if  self._length - index < max(self.window_size, self.order):
            target = []
        else:
            target = self.target[index]
        return sub_input_array, endogenous_array, target
    
    def __len__(self):
        
        return self._length
    
    def __next__(self):
        print(self._nb_iter)
        if self._is_dynamic_mode:
            endogenous = self.predicted
        else:
            endogenous = self.endogenous_signal
            
        if self._nb_iter < self._length- max(self.window_size, self.order):
            
            lower_lim = self._nb_iter 
            upper_lim = self._nb_iter + self.window_size
            sub_input_array = self.signal_input[lower_lim:upper_lim ]
            endog_upper_lim = self._nb_iter + self.order
            endogenous_array = endogenous[lower_lim:endog_upper_lim]
            target = self.target[self._nb_iter]
            self._nb_iter += 1                                   
            
            
        elif self._nb_iter < self._length :
            sub_input_array = self.signal_input[self._nb_iter : ]
            endogenous_array = endogenous[self._nb_iter :]
            target = []
            self._nb_iter += 1
            
        else:
            raise StopIteration('Maximum value reached')
        return sub_input_array, endogenous_array, target
    
    
    def predict(self, exogenous, endogenous):
        pass
    
    def update(self,  endogenous):
        
        self.predicted[self._nb_idx_predicted] = endogenous[0]
        self._nb_idx_predicted += 1
        self._nb_idx_predicted  = self._nb_idx_predicted % self.predicted.shape[0]
        
    def use_dynamic_mode(self, is_dynamic_mode = True):
        self._is_dynamic_mode = is_dynamic_mode
        
    def cross_validation(self, k=5):
        pass