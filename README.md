# Simple NARX (NON-LINEAR AUTOREGRESSIVE WITH EXOGENEOUS INPUT) model  with Pytorch framework

[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

A simple implementation of NARX model with Pytorch deep learning framework. NARX model
use endogenous and exogenous variable in order to make predictions. Basically, NARX model is an autoregressive
model, based on a MLP. 

**This work is still a work in progress.**
\begin{markdown}

We note y as endogenous variable (target variable), and x exogenous variable(s). 
w denotes the order of exogenous variable and d the order of endogenous variable
\begin{equation}
y(t) = f( y(t-1), ..., y(t-1-d), x(t-1), ..., x(t-1-w))
\end{equation}
\end{markdown}


## Getting Started

Please follow these instructions in order to run the model.
Below, an example of the model ability to predict temporal serie : the prediciton of volume (endogenous)
knwoing abdomen and thoracic contractions (exogenous).

\begin{markdown}
Here we can reference \textbf{Figure~\ref{fig:exampleimage}}



\setkeys{Gin}{width=.5\linewidth}

![exampleimage](images/signals.PNG "Time serie prediction example, with exogenous order of 10 and endogenous order of 2")
\end{markdown}

### Prerequisites

Run the following command on your command line prompt. You may change the way you install Pytorch, given your CPU/GPU and 
your operative system. 

```
$ pip install - r requirements.txt
```

### Installing

DataIter class formates endogenous and exogenous input for NARX model. 
You can set the model enodgenous order  and exogenous order, using order and widow_size arguments .

First get some data : with 2 exogenous variable
```
import numpy as np

len_signals = 10_000

lin = np.linspace(0,20, len_signals)
exog = np.concatenate([np.sin(lin)[:,np.newaxis],
                       np.sin(lin + np.pi/4)[:,np.newaxis]], axis=1)

endog = np.sin(lin + np.pi)[:,np.newaxis]
			   

```
Set exogenous order and endogenous order

```
exog_order = 10
endog_order = 5
```

Then use DataIter for formating data (returns ex : exogenous variable, endogenous variable, and target to be predicted)

```
from utils import DataIter
from model import Narx

di = DataIter(exog, endog,
window_size = exog_order, order = endog_order)
di.use_dynamic_mode()

predictions = []
narx = Narx([ exog_order, 2], order = endog_order)
for i, (ex, ed, target) in enumerate(di):
    if target:

        pred = narx(ex,ed)
        di.update(pred)
        predictions.append(pred)
```


Another example is available in the main.py file

# NARX model architecture

The model takes exogenous values and predicted values as input or target values as input. A scheme of the model
is given on the image below.

![exampleimage](images/NARX.PNG "NARX architecture")
 For performance sake, the model should take first real values as the endogenous variable for the first few points and then 
 use only predictions as endogenous variable. A method in DataIter will be coming soon to simplify this process.

## Built With

* [Pytorch framework](https://www.pytorch.org) - Deep learning framework


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


