import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import TimeSeriesSplit
import torch
import torch.nn as nn
import torch.nn.functional as F
from model import Narx
import sys

from utils import DataIter, normalize_sig

PATHNAME2 = r'data/yannick 07_04fin vni 7 0 20 cycles  essai 2 end.xls'



# loading data

data = pd.read_excel(PATHNAME2, header=[0,1], index_col=0)

data = data.rename(columns={"CH 1, Tho":"thorax", 
               "CH 2, Volume":"volume",
               "CH13, Abd":"abdomen", 
               "CH40, Debit":"flow"})

data = data.apply(normalize_sig)



############Training

# random seed

torch.manual_seed(1234)
np.random.seed(1234)

# for training, we are going to use a k-fold cross validation
n_epochs = 50
loss_function = nn.MSELoss()


# adam learning rate 

adam_lr = 0.0002



# decay of first order momentum of gradient
b1 = 0.5
# 2nd decay
b2 = 0.999


narx = Narx([10,2])
optimizer = torch.optim.Adam(narx.parameters(), lr=adam_lr, betas=(b1, b2))



tscv = TimeSeriesSplit(n_splits=5)
exog = np.array(data[["thorax", "abdomen"]])
endog = np.array(data["volume"])


all_predictions = []

try:
    for cv_i, (train_index, test_index) in enumerate(tscv.split(exog)):
        
        X_train, X_test = exog[train_index], exog[test_index]
        y_train, y_test = endog[train_index], endog[test_index]
        
        print("Training ( {}/5)".format(cv_i))
        di = DataIter(X_train, y_train)
        di.use_dynamic_mode()
        losses = []
        for epoch in range(n_epochs):
            predictions = []
            for i, (ex, ed, target) in enumerate(di):
                if target:
                    optimizer.zero_grad()
                    pred = narx(ex,ed)
                    di.update(pred)
                    loss = loss_function(pred, target)
                    loss.backward(retain_graph=False)
                    optimizer.step()

                    losses.append(loss.item())
    #             if i == 0:
    #                 print(ex)
    #                 print(ed)
    #                 print(target)
            print("epoch nb = {} / {} _ loss : {}".format(epoch, n_epochs, np.mean(losses)), end="\r")
            
        print("testing phase ( {}/5)".format(cv_i))
        # testing
        di_test = DataIter(X_test, y_test)
        # predicitons
        predictions = np.zeros(y_test.shape)
        for i_t, (ex, ed, target) in enumerate(di_test):
            if target:
                pred = narx(ex,ed)
                di_test.update(pred)
                predictions[i_t] = pred.detach().numpy()
        print(predictions.shape)
        
    
     
    all_predictions.append(np.square(predictions - y_test).mean(axis=0))
     
except :
    e = sys.exc_info()[0]
    with open("log.txt", "a") as myfile:
        
        myfile.write (e)
        myfile.write ("cvi : {}".format(cv_i))
        
finally:
    print("done")

    print(all_predictions)